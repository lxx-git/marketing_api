<?php

namespace Ow\MarketingApi\Huawei;

use Ow\MarketingApi\Huawei\Kernel\ServiceContainer;
use Ow\MarketingApi\Huawei\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Ow\MarketingApi\Huawei\Report\Report $report
 * @property \Ow\MarketingApi\Huawei\Kernel\Cache\Cache $cache
 * @package Ow\MarketingApi\Vivo
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        ReportServiceProvider::class
    ];
}
