<?php

namespace Ow\MarketingApi\Huawei\Kernel\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ow\MarketingApi\Huawei\Kernel\Exception\HuaweiException;

/**
 * Trait HttpRequest
 *
 * @package Ow\MarketingApi\Tencent\Traits
 */
trait HttpRequest
{

    /**
     * @throws GuzzleException
     * @throws HuaweiException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed
     */
    public function request(string $method, string $url, array $parameters = [])
    {

        $ret = $this->httpClient($this->defaults["http"])->request(
            $method,
            $url,
            $parameters
        );

        if(!$ret) throw new GuzzleException(json_encode(["msg" => "Huawei ads returned an error:", "code" => "-1"]));

        $content = json_decode($ret->getBody()->getContents(), 320);

        if (!$content) throw new HuaweiException(json_encode(["msg" => "Huawei ads return empty content", "code" => "-1"]));

        if ($content["code"] !== 0) throw new HuaweiException(json_encode($content, 320));

        return $content["data"];

    }

    /**
     * 实例化请求
     * @param array $basicParam
     * @return Client
     */
    public function httpClient(array $basicParam)
    {
        return new Client($basicParam);
    }
}
