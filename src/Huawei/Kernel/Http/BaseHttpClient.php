<?php

namespace Ow\MarketingApi\Huawei\Kernel\Http;

use Ow\MarketingApi\Huawei\Kernel\Exception\VivoException;
use Ow\MarketingApi\Huawei\Kernel\Traits\HttpRequest;

class BaseHttpClient
{
    /**
     * 实现 GuzzleHttp 请求方法
     * 并且使用 trait HttpRequest request 方法
     */
    use HttpRequest {
        request as HuaweiRequest;
    }

    /**
     * @var \Ow\MarketingApi\Huawei\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * 账号access_token account_id
     * @var array
     */
    protected $owner = [];

    /**
     * 默认全局配置
     * @var array
     */
    protected $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://ads.cloud.huawei.com/openapi/v2/'
        ]
    ];

    /**
     * 基础参数
     * @var array
     */
    protected $commonParameters = [];

    /**
     * BaseHttpClient constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $this->owner = [
            "access_token" => $app->defaultConfig["access_token"],
        ];
    }

    /**
     * @throws VivoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        //huawei 接口
        $url = $this->verificationRequestUri($url);

        return $this->HuaweiRequest($method, $url, [
            "json" => $parameters,
            "headers" => [
                "Content-Type"=> "application/json",
                "Authorization"=> "Bearer ". $this->owner["access_token"]
            ]
        ]);
    }

    /**
     * @param $url
     * @return string
     */
    public function verificationRequestUri($url)
    {
        $httpBasicInfo = $this->defaults["http"];

        $url = $httpBasicInfo["base_uri"] . $url;

        return $url;
    }

    /**
     * 多账号使用
     * 传入广告主id
     * @param int $access_token
     */
    public function setDefaultConfig($access_token)
    {
        $this->owner["access_token"] = $access_token;
    }
}