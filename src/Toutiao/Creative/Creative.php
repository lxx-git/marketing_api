<?php


namespace Ow\MarketingApi\Toutiao\Creative;

use Ow\MarketingApi\Toutiao\Auth\AccessToken;
use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Creative extends BaseHttpClient
{
    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function list(array $data) :array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);
        return $this->httpJsonGet('open_api/2/creative/get/',$data);
    }

    /**
     * 创建自定义创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function createCustomCreative(array $data) :array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
            'creative_list',
        ]);
        return $this->httpJsonPost('open_api/2/creative/custom_creative/create/',$data);
    }

    /**
     * 修改自定义创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateCustomCreative(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
            'creative_list',
        ]);
        return $this->httpJsonPost('open_api/2/creative/custom_creative/update/',$data);
    }

    /**
     * 创建程序化创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function createProceduralCreative(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
            'creative',
        ]);
        return $this->httpJsonPost('open_api/2/creative/procedural_creative/create/',$data);
    }

    /**
     * 修改程序化创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateProceduralCreative(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
            'creative',
        ]);
        return $this->httpJsonPost('open_api/2/creative/procedural_creative/update/',$data);
    }

    /**
     * 创建创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function create(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
        ]);

        return $this->httpJsonPost('open_api/2/creative/create_v2/',$data);
    }

    /**
     * 获取创意详细
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function read(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
        ]);

        return $this->httpJsonGet('open_api/2/creative/read_v2/',$data);
    }

    /**
     * 全量修改广告创意
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function update(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_id',
            'modify_time'
        ]);
        return $this->httpJsonPost('open_api/2/creative/update_v2/',$data);
    }

    /**
     * 修改创意状态
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateStatus(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'creative_ids',
            'opt_status'
        ]);

        return $this->httpJsonPost('open_api/2/creative/status/update_v2/',$data);
    }

    /**
     * 创意素材的详细信息
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function materialRead(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'creative_ids',
        ]);
        return $this->httpJsonGet('open_api/2/creative/material/read/',$data);
    }

    /**
     * 获取创意审核建议
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function rejectReason(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'creative_ids'
        ]);
        return $this->httpJsonGet('open_api/2/creative/reject_reason/',$data);
    }
}