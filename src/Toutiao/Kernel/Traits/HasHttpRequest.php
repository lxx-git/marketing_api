<?php


namespace Ow\MarketingApi\Toutiao\Kernel\Traits;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

trait HasHttpRequest
{
    /**
     * @var
     */
    protected $httpClient;

    protected $host = 'https://ad.oceanengine.com/';

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }


    /**
     * @return Client|ClientInterface
     */
    public function getHttpClient()
    {
        if (!($this->httpClient instanceof ClientInterface)) {
            if (property_exists($this, 'app') && $this->app['http_client']) {
                $this->httpClient = $this->app['http_client'];
            } else {
                $this->httpClient = new Client();
            }
        }

        return $this->httpClient;
    }

    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method,$url,array $options=[])
    {
        $options = array_merge(self::$defaults ?? [], $options);


        $res =  $this->getHttpClient()->request($method,$url,$options);

        if ( !$res ) throw new \Exception('request fail,no response return');
        return $res;
    }

    /**
     * @param string $uri
     * @param array $json
     * @param string $method
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJson(string $uri,array $json=[],string $method='POST')
    {
        return $this->request($method,$uri,['json'=>$json]);
    }

    /**
     * @param string $uri
     * @param array $json
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJsonPost(string $uri,array $json=[])
    {
        return $this->httpJson($uri,$json,'POST');
    }

    /**
     * @param string $uri
     * @param array $json
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJsonGet(string $uri,array $json=[])
    {
        return $this->httpJson($uri,$json,'GET');
    }


    /**
     * 有文件的上传
     * @param $uri
     * @param array $params
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function multipartRequest($uri,array $params=[]) : array
    {
        $multi = [];

        foreach ($params as $k=>$v){

            if ( is_string($v) || is_numeric($v) ){
                $multi[] = [
                    'name' => $k,
                    'contents' => $v,
                ];

            }elseif ( is_array($v) ){
                $multi[] = $v;
            }elseif( is_resource($v) ){
                $multi[] = [
                    'name' => $k,
                    'contents' => $v,
                ];
            }
        }

        $options = [
            'headers' => [
                'Content-Type' => 'multipart/form-data',

            ],
            'multipart' => $multi,
        ];

        return $this->request('POST',$uri,$options);
    }
}