<?php


namespace Ow\MarketingApi\Toutiao\Kernel;

use Ow\MarketingApi\Toutiao\Kernel\Interfaces\AccessTokenInterface;
use Ow\MarketingApi\Toutiao\Kernel\Traits\HasHttpRequest;

class AccessToken implements AccessTokenInterface
{

    use HasHttpRequest;

    protected $app;


    protected $authCodeTokenPath = 'open_api/oauth2/access_token/';//获取token

    protected $refreshTokenPath = 'open_api/oauth2/refresh_token/';//刷新token

    public function __construct($app)
    {
        $this->app = $app;

    }


    public function getToken()
    {
        return $this->app['config']->get('access_token');
    }

    /**
     * 获取token
     * @param $authCode
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function authCodeToken()
    {
        $res = $this->httpJsonPost($this->authCodeTokenPath,[
            'app_id' => $this->app['config']->get('appid'),
            'secret' => $this->app['config']->get('secret'),
            'grant_type' => 'auth_code',
            'auth_code' => $this->app['config']->get('auth_code'),
        ]);

        $content = $res->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        return $decode;
    }

    /**
     * 刷新token
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refreshToken()
    {
        $tokenInfo = $this->httpJsonPost($this->refreshTokenPath,[
            'app_id' => $this->app['config']->get('app_id'),
            'secret' => $this->app['config']->get('secret'),
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->app['config']->get('refresh_token'),
        ]);

        $content = $tokenInfo->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        return $decode;
    }


}