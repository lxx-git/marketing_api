<?php


namespace Ow\MarketingApi\Toutiao\Kernel\Http;


use Ow\MarketingApi\Helper\Arr;
use Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException;
use Ow\MarketingApi\Toutiao\Kernel\Interfaces\AccessTokenInterface;
use Ow\MarketingApi\Toutiao\Kernel\Traits\HasHttpRequest;

class BaseHttpClient
{
    use HasHttpRequest {
        request as perRequest;
    }
    protected $app;

    protected $accessToken;

    protected static $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ]
    ];

    public function __construct($app,AccessTokenInterface $accessToken=null)
    {
        $this->app = $app;

        $this->accessToken = $accessToken ?? $this->app->access_token;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function request($method, $url, array $options = [])
    {

        if ( !key_exists('headers',$options) || !key_exists('Access-Token',$options['headers']) ){
            $options['headers']['Access-Token'] = $this->accessToken->getToken();
        }

        if ( !key_exists('base_uri',$options) ){
            $options['base_uri'] = $this->getHost();
        }

        if ($this->app['config']->get('debug')){
            $options['headers']['X-Debug-Mode'] = $this->app['config']->get('debug');
        }
//        if ( $method == 'GET' ){
//            if ( !key_exists('query',$options) ) $options['query'] = [];
//
//            if ( !key_exists('advertiser_id',$options['query']) ) $options['query']['advertiser_id'] = $this->app['config']->get('advertiser_id') ?? '';
//
//        }elseif ( $method == 'POST'){
//
//        }

        $res = $this->perRequest($method,$url,$options);

        $content =  json_decode($res->getBody()->getContents(),true);

        if ( !$content || $content['code'] != 0 ) throw new \Exception(json_encode($content,JSON_UNESCAPED_UNICODE));

        return $content['data'];
    }

    public function validateRequiredParams(array $data,array $keys)
    {
        if ( !Arr::keys_all_exists($data,$keys) ) throw new ValidateRequestParamException('required keys :'.implode(',',$keys));
    }

}