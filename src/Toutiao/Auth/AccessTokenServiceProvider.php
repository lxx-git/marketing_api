<?php


namespace Ow\MarketingApi\Toutiao\Auth;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
class AccessTokenServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $pimple
     */
    public function register(Container $pimple)
    {
        !isset($pimple['access_token']) && $pimple['access_token'] = function ($app){
            return new AccessToken($app);
        };
    }
}