<?php


namespace Ow\MarketingApi\Toutiao;


use Ow\MarketingApi\Toutiao\Account\AccountServiceProvider;
use Ow\MarketingApi\Toutiao\Auth\AccessTokenServiceProvider;
use Ow\MarketingApi\Toutiao\Campaign\CampaignServiceProvider;
use Ow\MarketingApi\Toutiao\Creative\CreativeServiceProvider;
use Ow\MarketingApi\Toutiao\Kernel\ServiceContainer;
use Ow\MarketingApi\Toutiao\Material\MaterialServiceProvider;
use Ow\MarketingApi\Toutiao\Plan\PlanServiceProvider;
use Ow\MarketingApi\Toutiao\Report\ReportServiceProvider;
use Ow\MarketingApi\Toutiao\Tools\ToolsServiceProvider;

/**
 * Class Application
 * @property \Ow\MarketingApi\Toutiao\Account\Advertiser $advertiser
 * @property  \Ow\MarketingApi\Toutiao\Auth\AccessToken $access_token
 * @property \Ow\MarketingApi\Toutiao\Kernel\Cache\Cache $cache
 * @property \Ow\MarketingApi\Toutiao\Campaign\Campaign $campaign
 * @property \Ow\MarketingApi\Toutiao\Plan\Plan $plan
 * @property \Ow\MarketingApi\Toutiao\Creative\Creative $creative
 * @property \Ow\MarketingApi\Toutiao\Report\Report $report
 * @property \Ow\MarketingApi\Toutiao\Material\Material $material
 * @property \Ow\MarketingApi\Toutiao\Material\Image $image
 * @property \Ow\MarketingApi\Toutiao\Material\Video $video
 * @property \Ow\MarketingApi\Toutiao\Tools\Tools $tools
 * @package Ow\MarketingApi\Toutiao
 */
class Application extends ServiceContainer
{
    protected $providers = [
        AccessTokenServiceProvider::class,
        AccountServiceProvider::class,
        CampaignServiceProvider::class,
        PlanServiceProvider::class,
        CreativeServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
        ToolsServiceProvider::class
    ];

    public function __get($name)
    {
        return $this->offsetGet($name);
    }
}