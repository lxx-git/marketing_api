<?php


namespace Ow\MarketingApi\Toutiao\Account;


use Pimple\Container;
use  Pimple\ServiceProviderInterface;
class AccountServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        !isset($pimple['advertiser']) && $pimple['advertiser'] = function ($app){
            return new Advertiser($app);
        };
    }
}