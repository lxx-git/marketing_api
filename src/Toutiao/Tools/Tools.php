<?php


namespace Ow\MarketingApi\Toutiao\Tools;


use Ow\MarketingApi\Toutiao\Kernel\Http\BaseHttpClient;

class Tools extends BaseHttpClient
{
    /**
     * 行动号召字段内容获取
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function actionText(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'landing_type'
        ]);
        return $this->httpJsonGet('open_api/2/tools/action_text/get/',$data);
    }

    /**
     * 获取计划学习状态
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function adStatExtraInfo(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_ids'
        ]);
        return $this->httpJsonGet('open_api/2/tools/ad_stat_extra_info/get/',$data);
    }

    /**
     * 获取计划诊断信息(新)
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function diagnosis(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'ad_ids'
        ]);

        return $this->httpJsonGet('open_api/2/tools/diagnosis/ad/get_v2/',$data);
    }

    /**
     * 获取行业列表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function industry(array $data):array
    {
        return $this->httpJsonGet('open_api/2/tools/industry/get/',$data);
    }

    /**
     * 转化目标列表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function convertList(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id'
        ]);
        return $this->httpJsonGet('open_api/2/tools/adv_convert/select/',$data);
    }

    /**
     * 创建转化目标
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function createConvert(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'name'
        ]);
        return $this->httpJsonPost('open_api/2/tools/ad_convert/create/',$data);
    }

    /**
     * 获取应用详情
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function getAppInfo(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);
        return $this->httpJsonPost('open_api/2/tools/app_management/app/get/',$data);
    }


    /**
     * 获取推广信息
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function getRecommend(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'text_type',
            'industry_id',
        ]);
        return $this->httpJsonPost('open_api/2/tools/promotion_card/recommend_title/get/',$data);
    }


    /**
     * 获取创意组件列表
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
     */
    public function creativeComponent(array $data):array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);
        return $this->httpJsonPost('open_api/2/assets/creative_component/get/',$data);
    }

    /**
    * 获取橙子建站列表
    * @param array $data
    * @return array
    * @throws \GuzzleHttp\Exception\GuzzleException
    * @throws \Ow\MarketingApi\Toutiao\Kernel\Exceptions\ValidateRequestParamException
    */
    public function getSite(array $data):array
    {
        $this->validateRequiredParams($data,[

            'advertiser_id',
            'page',
            'page_size',
            'status' => "SITE_ONLINE",
        ]);
        return $this->httpJsonPost('open_api/2/tools/site/get/',$data);
    }
}