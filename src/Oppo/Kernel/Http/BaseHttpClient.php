<?php

namespace Ow\MarketingApi\Oppo\Kernel\Http;

use Ow\MarketingApi\Oppo\Kernel\Exception\OppoException;
use Ow\MarketingApi\Oppo\Kernel\Traits\HttpRequest;

class BaseHttpClient
{
    /**
     * 实现 GuzzleHttp 请求方法
     * 并且使用 trait HttpRequest request 方法
     */
    use HttpRequest {
        request as OppoRequest;
    }

    /**
     * @var \Ow\MarketingApi\Oppo\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * 账号access_token account_id
     * @var array
     */
    protected $owner = [];

    /**
     * 默认全局配置
     * @var array
     */
    protected $defaults = [
        'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://sapi.ads.oppomobile.com/v3/'
        ]
    ];

    /**
     * 基础参数
     * @var array
     */
    protected $commonParameters = [];

    /**
     * BaseHttpClient constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $defaultConfig = $app->defaultConfig;
        $this->owner = [
            "api_id" => $defaultConfig["api_id"],
            "api_key" => $defaultConfig["api_key"],
            "ownerId" => $defaultConfig["owner_id"],
        ];
    }

    /**
     * @throws OppoException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        //oppo加密参数
        $sign = $this->sign($this->owner);

        return $this->OppoRequest($method, $url, [
            "form_params" => $parameters,
            'headers' => [
                'Authorization' => "Bearer " . $sign
            ]
        ]);
    }

    /**
     * oppo header
     * @param $owner
     * @return string Authorization: Bearer
     */
    public function sign($owner)
    {
        $time = time();

        $sign = sha1($owner["api_id"] . $owner["api_key"] . $time);

        return base64_encode($owner["ownerId"] . "," . $owner["api_id"] . "," . $time . "," . $sign);
    }

    /**
     * 多账号
     * 传入账号的所有参数
     * @param array $config
     */
    public function setDefaultConfig(array $config)
    {
        $this->owner = [
            "api_id" => $config["api_id"],
            "api_key" => $config["api_key"],
            "ownerId" => $config["owner_id"],
        ];
    }
}