<?php

namespace Ow\MarketingApi\BaiDu\Tool;

use Ow\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;

class Tool extends BaseHttpClient
{
    /**
     * 账户列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\BaiDu\Kernel\Exceptions\ValidateRequestParamException
     */
    public function userLists(array $data)
    {
        return $this->httpJsonPost('/MccFeedService/getUserListByMccid',$data);
    }

    /**
     * 获取账户信息
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userInfo(array $data){

        return $this->httpJsonPost('/AccountFeedService/getAccountFeed',$data);
    }

    /**
     * 获取app列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAppList(array $data)
    {
        return $this->httpJsonPost('/MccFeedService/getUserListByMccid',$data);
    }


    /**获取精选媒体包
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getMediaPackages(array $data)
    {
        return $this->httpJsonPost('/SearchFeedService/getMediaPackages',$data);

    }

    /**获取ocpc转化追踪
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOcpcTransFeed(array $data)
    {
        return $this->httpJsonPost('/SearchFeedService/getOcpcTransFeed',$data);
    }

    /**获取定向包
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAtpFeed(array $data)
    {
        return $this->httpJsonPost('/AtpFeedService/getAtpFeed',$data);
    }

    /**添加定向包
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addAtpFeed(array $data)
    {
        return $this->httpJsonPost("/AtpFeedService/addAtpFeed",$data);
    }

    /**更新定向包字段
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateAtpFeed(array $data)
    {
        return $this->httpJsonPost("/AtpFeedService/updateAtpFeed",$data);
    }

    /**绑定定向包
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function bindAtpFeed(array $data)
    {
        return $this->httpJsonPost("/AtpFeedService/bindAtpFeed",$data);
    }

    /**根据APP名称模糊查询APPID，用于APP偏好定向设置
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAppIdName(array $data)
    {
        return $this->httpJsonPost("/SearchFeedService/getAppIdNameFeed",$data);
    }


    /**
     * 资产分享
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function share(array $data)
    {
        return $this->httpJsonPost("/AssetShareFeedService/addAssetShareFeed",$data);
    }
}