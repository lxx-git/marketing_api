<?php
namespace Ow\MarketingApi\BaiDu\Kernel\Http;


use Ow\MarketingApi\Helper\Arr;
use Ow\MarketingApi\BaiDu\Kernel\Exceptions\ValidateRequestParamException;
use Ow\MarketingApi\BaiDu\Kernel\Interfaces\AccessTokenInterface;
use Ow\MarketingApi\BaiDu\Kernel\Traits\HasHttpRequest;
use Ow\MarketingApi\BaiDu\Kernel\Traits\RequestSandbox;
use function Couchbase\defaultDecoder;

class BaseHttpClient
{

    use HasHttpRequest {
        request as perRequest;
    }
    use RequestSandbox;

    /**
     * @var \Ow\MarketingApi\BaiDu\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * @var \Ow\MarketingApi\BaiDu\Kernel\Interfaces\AccessTokenInterface
     */
    protected $accessToken;

    protected static $defaults = [
        'headers' => [
            'Content-Type' => 'application/x-www-form-urlencoded',
        ]
    ];

    public function __construct($app)
    {
        $this->app = $app;
        $type = $this->app['config']->get('type');

        if($type == 'video' || $type == 'image'){
            $this->useSandbox(false);
        }
    }


    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $url, array $options = [])
    {

        $base_url = $this->getHost();

        $url = $base_url.$url;

        $res = $this->perRequest($method,$url,$options);

        $content =  json_decode($res->getBody()->getContents(),320);

        if($content["header"]["status"] !== 0) throw new \Exception(json_encode($content['header']['failures'],JSON_UNESCAPED_UNICODE));

        return $content['body'];
    }


    public function validateRequiredParams(array $data,array $keys)
    {
        if ( !Arr::keys_all_exists($data,$keys) ) throw new ValidateRequestParamException('required keys :'.implode(',',$keys));
    }



}