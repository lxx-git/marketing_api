<?php

namespace Ow\MarketingApi\BaiDu\Kernel;

use Ow\MarketingApi\BaiDu\Kernel\Interfaces\AccessTokenInterface;
use Ow\MarketingApi\BaiDu\Kernel\Traits\HasHttpRequest;
use Ow\MarketingApi\BaiDu\Kernel\Traits\RequestSandbox;

class AccessToken implements AccessTokenInterface
{

    use HasHttpRequest;
    use RequestSandbox;


    /**
     * @var \Ow\MarketingApi\Kuaishou\Kernel\ServiceContainer;
     */
    protected $app;

    protected $storeAccessTokenKey = 'ow:baidu:access_token';

    protected $storeRefreshTokenKey = 'ow:baidu:refresh_token';

    protected $cacheAdvertiserIdPrefix = 'ow:baidu:advertiser_id:';

    protected $cacheAdvertiserIdsPrefix = 'ow:baidu:advertiser_ids:';

    protected $authCode2TokenPath = 'rest/openapi/oauth2/authorize/access_token';

    protected $refreshTokenPath = 'rest/openapi/oauth2/authorize/refresh_token';


    public function __construct($app)
    {
        $this->app = $app;
    }

    public function getAdvertiserId() : string
    {

        $cacheKey = $this->getCacheAdvertiserIdKey();
        $cache = $this->getCache();
        if ( $cache->has($cacheKey) && $res = $cache->get($cacheKey) ) return $res;

        return '';
    }

    /**
     * @return string
     */
    public function getCacheAdvertiserIdKey() : string
    {
        return $this->cacheAdvertiserIdPrefix . $this->app['config']->get('appid');
    }

    public function getCacheAdvertiserIdsKey() : string
    {
        return $this->cacheAdvertiserIdsPrefix . $this->app['config']->get('appid');
    }

    public function getAdvertiserIds() : array
    {

        $cacheKey = $this->getCacheAdvertiserIdsKey();
        $cache = $this->getCache();

        if ( $cache->has($cacheKey) && $ids = $cache->SMEMBERS($cacheKey) ) return $ids;

        return [];
    }

    public function getToken($refresh=false)
    {
        $cache = $this->getCache();
        $cacheKey = $this->getStoreTokenKey();

        if ( $refresh === false ){
            if ( $cache->has($cacheKey) && $res = $cache->get( $cacheKey ) )
                return $res;
        }

        //是否有refresh token
        $refreshToken = $this->getRefreshToken();

        if ( !$refreshToken ) throw new \Exception('no refresh token,please try to get auth code');

        $data = $this->refreshToken2AccessToken($refreshToken);

        return $data['access_token'];
    }

    public function authCode2ToKen($authCode)
    {
        $res = $this->httpJsonPost($this->authCode2TokenPath,[
            'app_id' => $this->app['config']->get('appid'),
            'secret' => $this->app['config']->get('secret'),
            'auth_code' => $authCode,
        ]);

        $content = $res->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setTokenInfo($decode['data']);

        return $decode['data'];
    }

    /**
     * @return \Ow\MarketingApi\Kuaishou\Kernel\Cache\Cache
     */
    public function getCache()
    {
        return $this->app['cache'];
    }

    public function setToken($token,$lifetime=3600)
    {
        $this->getCache()->set($this->getStoreTokenKey(),$token,$lifetime);
    }

    public function setRefreshToken($token,$lifetime=86400)
    {
        $this->getCache()->set($this->getRefreshTokenKey(),$token,$lifetime);
    }

    public function getRefreshToken()
    {
        $cache = $this->getCache();
        $key = $this->getRefreshTokenKey();

        return $cache->has($key) ? $cache->get($key) : null;
    }

    public function setTokenInfo(array $data)
    {
        $this->setToken($data['access_token'],$data['access_token_expires_in']);

        $this->setRefreshToken($data['refresh_token'],$data['refresh_token_expires_in']);

        $this->setAdvertiserId($data['advertiser_id']);

        $this->setAdvertiserIds($data['advertiser_ids'] ?? []);
    }

    /**
     * @param $advertiserId
     * @return $this
     */
    public function setAdvertiserId($advertiserId) : self
    {

        $cacheKey = $this->getCacheAdvertiserIdKey();
        $cache = $this->getCache();

        $cache->forever($cacheKey,$advertiserId);

        return $this;
    }

    /**
     * @param array $ids
     * @return $this
     */
    public function setAdvertiserIds(array $ids) : self
    {
        $cacheKey = $this->getCacheAdvertiserIdsKey();
        $cache = $this->getCache();

        $cache->SAdd($cacheKey,...$ids);

        return $this;
    }

    public function refreshToken2AccessToken($refreshToken='')
    {
        if ( empty($refreshToken) ){
             $refreshToken = $this->getRefreshToken();
        }

        if ( !$refreshToken ) throw new \Exception('no refresh token, please try to get auth code');

        $tokenInfo = $this->httpJsonPost($this->refreshTokenPath,[
            'app_id' => $this->app['config']->get('appid'),
            'secret' => $this->app['config']->get('secret'),
            'refresh_token' => $refreshToken,
        ]);

        $content = $tokenInfo->getBody()->getContents();

        if ( !$content ) throw new \Exception('nothing response');

        $decode = json_decode($content,true);

        if ( $decode === false ) throw new \Exception('response error:'.$content);

        if ( $decode['code'] !== 0 ) throw new \Exception('fail:'.$content);

        $this->setTokenInfo($decode['data']);

        return $decode['data'];
    }

    public function refresh(): AccessTokenInterface
    {

        $this->getToken(true);

        return $this;
    }

    public function getStoreTokenKey() : string
    {
        return $this->storeAccessTokenKey . ':'. $this->app['config']->get('appid') ;
    }

    public function getRefreshTokenKey() : string
    {
        return $this->storeRefreshTokenKey . ':'. $this->app['config']->get('appid') ;
    }

}