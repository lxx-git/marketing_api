<?php

namespace Ow\MarketingApi\BaiDu\Material;

use Ow\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;
use Ow\MarketingApi\BaiDu\Kernel\Traits\RequestSandbox;
class Image extends BaseHttpClient
{

    /**上传图片
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function uploadImg(array $data)
    {
        return $this->httpJsonPost("/ImageManageService/uploadImage",$data);
    }

    /**获取账户下的图片列表
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getImage(array $data)
    {
        return $this->httpJsonPost("/ImageManageService/getImageList",$data);
    }

    /**编辑图片 （收藏，改名）
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function moImage(array $data)
    {
        return $this->httpJsonPost("/ImageManageService/modImage",$data);
    }
}