<?php
namespace Ow\MarketingApi\BaiDu\Creative;

use Ow\MarketingApi\BaiDu\Kernel\Http\BaseHttpClient;

class Creative extends BaseHttpClient
{

    /**
     * 获取广告创意
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCreative(array $data)
    {
        return $this->httpJsonPost("/CreativeFeedService/getCreativeFeed",$data);
    }

    /**
     * 创建广告创意
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createCreative(array $data)
    {
        return $this->httpJsonPost("CreativeFeedService/addCreativeFeed",$data);
    }
}