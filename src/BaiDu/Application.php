<?php

namespace Ow\MarketingApi\BaiDu;

use Ow\MarketingApi\BaiDu\Account\AccountServiceProvider;
use Ow\MarketingApi\BaiDu\Material\MaterialServiceProvider;
use Ow\MarketingApi\BaiDu\Campaign\CampaignServiceProvider;
use Ow\MarketingApi\BaiDu\Group\GroupServiceProvider;
use Ow\MarketingApi\BaiDu\Creative\CreativeServiceProvider;
use Ow\MarketingApi\BaiDu\Kernel\ServiceContainer;
use Ow\MarketingApi\BaiDu\Report\ReportServiceProvider;
use Ow\MarketingApi\BaiDu\Tool\ToolServiceProvider;

//use Ow\MarketingApi\BaiDu\Material\MaterialServiceProvider;
//use Ow\MarketingApi\BaiDu\Plan\PlanServiceProvider;
//use Ow\MarketingApi\BaiDu\Report\ReportServiceProvider;


/**
 * Class Application
 * @property \Ow\MarketingApi\BaiDu\Account\Advertiser $advertiser
 * @property \Ow\MarketingApi\BaiDu\Tool\Tool $tool
 * @property \Ow\MarketingApi\BaiDu\Campaign\Campaign $campaign
 * @property \Ow\MarketingApi\BaiDu\Group\Group $group
 * @property \Ow\MarketingApi\BaiDu\Creative\Creative $creative
 * @property \Ow\MarketingApi\BaiDu\Report\Report $report
// * @property \Ow\MarketingApi\BaiDu\Material\Material $material
 * @property \Ow\MarketingApi\BaiDu\Material\Video $video
 * @property \Ow\MarketingApi\BaiDu\Material\Image $image
 * @package Ow\MarketingApi\BaiDu
 */
class Application extends ServiceContainer
{

    protected $providers = [
//        AccessTokenServiceProvider::class,
        ToolServiceProvider::class,
        CampaignServiceProvider::class,
        GroupServiceProvider::class,
        CreativeServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
    ];

    public function __get($name)
    {
        return $this->offsetGet($name);
    }

}