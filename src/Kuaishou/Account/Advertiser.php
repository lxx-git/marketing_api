<?php

namespace Ow\MarketingApi\Kuaishou\Account;

use Ow\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Advertiser extends BaseHttpClient
{

    /**
     * 获取广告账户信息
     * @param $advertiserId
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/2.1.1
     */
    public function info($advertiserId) : array
    {
        return $this->httpJsonGet('rest/openapi/v1/advertiser/info',[
            'advertiser_id' => $advertiserId
        ]);
    }

    /**
     * 获取广告账户余额信息
     * @param $advertiserId
     * @return array|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/2.1.2
     */
    public function fund($advertiserId) : array
    {
        return $this->httpJsonGet('rest/openapi/v1/advertiser/fund/get',[
            'advertiser_id' => $advertiserId
        ]);
    }

    /**
     * 获取广告账户流水信息
     * @param $advertiserId
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * https://developers.e.kuaishou.com/docs/2.1.3
     */
    public function dailyFlows($advertiserId,array $data) : array
    {
        $data['advertiser_id'] = $advertiserId;

        return $this->httpJsonGet('rest/openapi/v1/advertiser/fund/get',$data);
    }

    /**
     * 账户操作记录信息查询
     * @param $advertiserId
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @see https://developers.e.kuaishou.com/docs/3.1.6
     */
    public function recordList($advertiserId,array $data=[]) : array
    {
        $data['advertiser_id'] = $advertiserId;

        return $this->httpJsonPost('rest/openapi/v1/advertiser/fund/get',$data);
    }
}