<?php

namespace Ow\MarketingApi\Kuaishou\Plan;

use Ow\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Plan extends BaseHttpClient
{

    /**
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.1.2
     */
    public function lists(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/ad_unit/list',$data);
    }

    /**
     * @param array $data
     * advertiser_id int required
     * campaign_id int required
     * unit_name string required
     * bid_type int required
     * put_status int options
     * template_id int options
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException|\Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.4.1
     */
    public function create(array $data) : array
    {

        $this->validateRequiredParams($data,[
            'advertiser_id',
            'campaign_id',
            'unit_name',
            'bid_type',
            'put_status',
            'scene_id',
            'unit_type',
            'begin_time',
            'day_budget_schedule',
            'show_mode',
            'speed',
            'target',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/ad_unit/create',$data);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.4.3
     */
    public function update(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'unit_name',
            'scene_id',
            'begin_time',
            'show_mode',
            'speed',
            'target',
        ]);

        return $this->httpJsonPost('rest/openapi/v2/ad_unit/update',$data);

    }

    /**
     * 修改预算
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.4.5
     */
    public function updateDayBudget(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'day_budget',
            'day_budget_schedule',
        ]);


        return $this->httpJsonPost('rest/openapi/v1/ad_unit/update/day_budget',$data);
    }

    /**
     * 修改状态
     * @param array $data
     * <p>unit_ids int[] options </p>
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/3.4.6
     */
    public function updateStatus(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'put_status',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/ad_unit/update/status',$data);
    }

    /**
     * 修改广告组出价
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     */
    public function updateBid(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'unit_id',
            'bid',
        ]);

        return $this->httpJsonPost('rest/openapi/v1/ad_unit/update/bid',$data);
    }

}