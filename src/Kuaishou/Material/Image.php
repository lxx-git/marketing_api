<?php

namespace Ow\MarketingApi\Kuaishou\Material;

use Ow\MarketingApi\Kuaishou\Kernel\Http\BaseHttpClient;

class Image extends BaseHttpClient
{

    /**
     * 上传图片
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/5.1.2
     */
    public function upload(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'upload_type',
        ]);

        if ( $data['upload_type'] == 1 ){
            $this->validateRequiredParams($data,[
                'file',
                'signature'
            ]);
        }elseif($data['upload_type'] == 2){
            $this->validateRequiredParams($data,[
                'url',
            ]);
        }

        return $this->multipartRequest('rest/openapi/v2/file/ad/image/upload',$data);
    }


    /**
     * 查询单张图片的信息
     * @param array $data
     * @return array|Image
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/5.1.3
     */
    public function detail(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
            'image_token',
        ]);

        return $this-$this->httpJsonGet('rest/openapi/v1/file/ad/image/get',$data);
    }

    /**
     * 查询图片接口list接口，分页
     * @param array $data start_date(Y-m-d)、end_date、page、page_size
     * @return array|Image
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException
     * @see https://developers.e.kuaishou.com/docs/5.1.4
     */
    public function lists(array $data) : array
    {
        $this->validateRequiredParams($data,[
            'advertiser_id',
        ]);

        return $this-$this->httpJsonGet('rest/openapi/v1/file/ad/image/list',$data);
    }
}