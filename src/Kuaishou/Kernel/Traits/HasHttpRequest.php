<?php


namespace Ow\MarketingApi\Kuaishou\Kernel\Traits;


use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\HandlerStack;

trait HasHttpRequest
{

    /**
     * @var \GuzzleHttp\ClientInterface
     */
    protected $httpClient;

    protected $middlewares = [];

    protected $handlerStack ;

//    protected static $defaults = [];

    /**
     * @param string $uri
     * @param array $json
     * @param string $method
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJson(string $uri,array $json=[],string $method='POST')
    {
        return $this->request($method,$uri,['json'=>$json]);
    }

    /**
     * @param string $uri
     * @param array $json
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJsonPost(string $uri,array $json=[])
    {
        return $this->httpJson($uri,$json,'POST');
    }

    /**
     * @param string $uri
     * @param array $json
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function httpJsonGet(string $uri,array $json=[])
    {
        return $this->httpJson($uri,$json,'GET');
    }

    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method,$url,array $options=[])
    {
        $options = array_merge(self::$defaults ?? [], $options, ['handler' => $this->getHandlerStack()]);

//        $this->pushMiddleware($this->accessTokenMiddleware(), 'access_token');

        $res =  $this->getHttpClient()->request($method,$url,$options);

        if ( !$res ) throw new \Exception('request fail,no response return');

        return $res;
    }

    /**
     * 有文件的上传
     * @param $uri
     * @param array $params
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function multipartRequest($uri,array $params=[]) : array
    {
        $multi = [];

        foreach ($params as $k=>$v){

            if ( is_string($v) || is_numeric($v) ){
                $multi[] = [
                    'name' => $k,
                    'contents' => $v,
                ];

            }elseif ( is_array($v) ){
                $multi[] = $v;
            }elseif( is_resource($v) ){
                $multi[] = [
                    'name' => $k,
                    'contents' => $v,
                ];
            }
        }

        $options = [
            'headers' => [
                'Content-Type' => 'multipart/form-data',

            ],
            'multipart' => $multi,
        ];

        return $this->request('POST',$uri,$options);
    }

    /**
     * Attache access token to request query.
     *
     * @return \Closure
     */
//    protected function accessTokenMiddleware()
//    {
//        return function (callable $handler) {
//            return function (RequestInterface $request, array $options) use ($handler) {
//                if ($this->accessToken) {
//                    $request = $this->accessToken->applyToRequest($request, $options);
//                }
//
//                return $handler($request, $options);
//            };
//        };
//    }

    /**
     * @return Client|ClientInterface|mixed
     */
    public function getHttpClient()
    {
        if (!($this->httpClient instanceof ClientInterface)) {
            if (property_exists($this, 'app') && $this->app['http_client']) {
                $this->httpClient = $this->app['http_client'];
            } else {
//                $this->httpClient = new Client(['handler' => HandlerStack::create($this->getGuzzleHandler())]);
                $this->httpClient = new Client();
            }
        }

        return $this->httpClient;
    }

 
    /**
     * Add a middleware.
     *
     * @param string $name
     *
     * @return $this
     */
    public function pushMiddleware(callable $middleware, string $name = null)
    {
        if (!is_null($name)) {
            $this->middlewares[$name] = $middleware;
        } else {
            array_push($this->middlewares, $middleware);
        }

        return $this;
    }

    /**
     * Build a handler stack.
     */
    public function getHandlerStack(): HandlerStack
    {
        if ($this->handlerStack) {
            return $this->handlerStack;
        }

        $this->handlerStack = HandlerStack::create($this->getGuzzleHandler());

        foreach ($this->middlewares as $name => $middleware) {
            $this->handlerStack->push($middleware, $name);
        }

        return $this->handlerStack;
    }

    /**
     * Get guzzle handler.
     *
     * @return callable
     */
    protected function getGuzzleHandler()
    {
        if (property_exists($this, 'app') && isset($this->app['guzzle_handler'])) {
            return is_string($handler = $this->app->raw('guzzle_handler'))
                ? new $handler()
                : $handler;
        }

        return \GuzzleHttp\choose_handler();
    }
}