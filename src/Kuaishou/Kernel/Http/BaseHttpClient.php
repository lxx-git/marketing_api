<?php
namespace Ow\MarketingApi\Kuaishou\Kernel\Http;


use Ow\MarketingApi\Helper\Arr;
use Ow\MarketingApi\Kuaishou\Kernel\Exceptions\ValidateRequestParamException;
use Ow\MarketingApi\Kuaishou\Kernel\Interfaces\AccessTokenInterface;
use Ow\MarketingApi\Kuaishou\Kernel\Traits\HasHttpRequest;
use Ow\MarketingApi\Kuaishou\Kernel\Traits\RequestSandbox;

class BaseHttpClient
{

    use HasHttpRequest {
        request as perRequest;
    }
    use RequestSandbox;

    /**
     * @var \Ow\MarketingApi\Kuaishou\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * @var \Ow\MarketingApi\Kuaishou\Kernel\Interfaces\AccessTokenInterface
     */
    protected $accessToken;

    protected static $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ]
    ];

    public function __construct($app,AccessTokenInterface $accessToken=null)
    {
        $this->app = $app;
        $this->accessToken = $accessToken ?? $app['access_token'];

        $this->useSandbox($this->app['config']->get('sandbox',false));
    }


    public function getAccessToken()
    {
        return $this->accessToken;
    }


    /**
     * @param $method
     * @param $url
     * @param array $options
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($method, $url, array $options = [])
    {

        if ( !key_exists('headers',$options) || !key_exists('Access-Token',$options['headers']) ){
            $options['headers']['Access-Token'] = $this->accessToken->getToken();
        }

        if ( !key_exists('base_uri',$options) ){
            $options['base_uri'] = $this->getHost();
        }

        $res = $this->perRequest($method,$url,$options);

        $content =  json_decode($res->getBody()->getContents(),true);

        if ( !$content || $content['code'] != 0 ) throw new \Exception(json_encode($content,JSON_UNESCAPED_UNICODE));

        return $content['data'];
    }


    public function validateRequiredParams(array $data,array $keys)
    {
        if ( !Arr::keys_all_exists($data,$keys) ) throw new ValidateRequestParamException('required keys :'.implode(',',$keys));
    }



}