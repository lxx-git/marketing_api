<?php

namespace Ow\MarketingApi\Kuaishou\Kernel\Cache;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class CacheServiceProvider implements ServiceProviderInterface
{

    public function register(Container $pimple)
    {
        !isset($pimple['cache']) && $pimple['cache'] = function (){
            return new Cache();
        };
    }
}