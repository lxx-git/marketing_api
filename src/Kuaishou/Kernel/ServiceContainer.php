<?php

namespace Ow\MarketingApi\Kuaishou\Kernel;

use Ow\MarketingApi\Kuaishou\Kernel\Cache\CacheServiceProvider;
use Ow\MarketingApi\Kuaishou\Kernel\Providers\ConfigServiceProvider;
use Ow\MarketingApi\Kuaishou\Kernel\Providers\HttpClientServiceProvider;
use Ow\MarketingApi\Kuaishou\Kernel\Providers\RequestServiceProvider;
use Pimple\Container;

class ServiceContainer extends Container
{

    protected $defaultConfig = [];

    protected $userConfig = [];

    protected $providers = [];

    public function __construct($config = [],array $values = [])
    {
        $this->userConfig = $config;

        parent::__construct($values);

        $this->registerProviders($this->getProviders());
    }

    public function getConfig() : array
    {

        $base = [
            'http' => [
                'timeout' => 30,
                'base_uri' =>  'https://ad.e.kuaishou.com/',
            ]
        ];

        return array_replace_recursive($base,$this->defaultConfig,$this->userConfig);
    }

    public function getProviders() : array
    {
        return array_merge([
            ConfigServiceProvider::class,
            HttpClientServiceProvider::class,
//            RequestServiceProvider::class,
            CacheServiceProvider::class,
        ],$this->providers);
    }


    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}