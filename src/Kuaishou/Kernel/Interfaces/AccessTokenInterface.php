<?php


namespace Ow\MarketingApi\Kuaishou\Kernel\Interfaces;


interface AccessTokenInterface
{

    public function getToken();

    public function refresh() : self;

}