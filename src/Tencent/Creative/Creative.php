<?php

namespace Ow\MarketingApi\Tencent\Creative;

use Ow\MarketingApi\Tencent\CommonActions\Operate;

class Creative extends Operate
{
    protected $interface = "adcreatives";
}