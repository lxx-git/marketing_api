<?php

namespace Ow\MarketingApi\Tencent\ProductCatalogs;

use Ow\MarketingApi\Tencent\Kernel\Http\BaseHttpClient;

class ProductCatalogs extends BaseHttpClient
{
    /**
     * 获取上商品库
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Ow\MarketingApi\Tencent\Kernel\Exception\TencentException
     * @param array $parameters
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function get(array $parameters)
    {
        return $this->request("GET", "product_catalogs/get", $parameters);
    }
}