<?php

namespace Ow\MarketingApi\Tencent\Plan;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class PlanServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['plan'] = function ($app) {
            return new Plan($app);
        };
    }
}