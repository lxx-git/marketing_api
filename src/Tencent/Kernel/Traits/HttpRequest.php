<?php

namespace Ow\MarketingApi\Tencent\Kernel\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Ow\MarketingApi\Tencent\Kernel\Exception\TencentAdsResponseException;
use Ow\MarketingApi\Tencent\Kernel\Exception\TencentException;

/**
 * Trait HttpRequest
 *
 * @package Ow\MarketingApi\Tencent\Traits
 */
trait HttpRequest
{

    /**
     * @throws GuzzleException
     * @throws TencentException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        try {
            $ret = $this->httpClient($this->defaults)->request(
                $method,
                $url,
                $parameters
            );
        } catch (GuzzleException $e) {
            throw new GuzzleException(json_encode(["msg" => "Tencent ads returned an error:", "code" => "-1"]));
        }

        $content = json_decode($ret->getBody()->getContents(), 320);

        if (!$content) throw new TencentException(json_encode(["msg" => "Tencent ads return empty content", "code" => "-1"]));

        if ($content["code"] !== 0) throw new TencentException(json_encode($content, 320));

        return $content["data"];

    }

    /**
     * 上传
     * @throws GuzzleException
     * @throws TencentException
     * @param $url
     * @param array $params
     * @return array
     */
    public function multipartRequest($url,array $params=[]) : array
    {
        $params["account_id"] = $params["account_id"] ?? $this->owner["account_id"];
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);  // 从证书中检查SSL加密算法是否存在
        curl_setopt($curl, CURLOPT_POSTFIELDS, $params);
        try {
            $result = curl_exec($curl);
        } catch (\Exception $exception){
            throw new GuzzleException(json_encode(["msg" => "Tencent ads returned an error:", "code" => "-1"]));
        }
        curl_close($curl);

        $content = json_decode($result, 320);

        if (!$content) throw new TencentException(json_encode(["msg" => "Tencent ads return empty content", "code" => "-1"]));

        if ($content["code"] !== 0) throw new TencentException(json_encode($content, 320));

        return $content["data"];
    }

    /**
     * 实例化请求
     * @return Client
     * @param $config
     */
    public function httpClient($config)
    {
        return new Client($config);
    }
}
