<?php

namespace Ow\MarketingApi\Tencent\Kernel\Interfaces;

interface AccessTokenInterface
{
    public function getToken();
}