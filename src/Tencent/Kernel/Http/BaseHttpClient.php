<?php

namespace Ow\MarketingApi\Tencent\Kernel\Http;

use Ow\MarketingApi\Tencent\Kernel\Exception\TencentException;
use Ow\MarketingApi\Tencent\Kernel\Traits\HttpRequest;

class BaseHttpClient
{
    /**
     * 实现 GuzzleHttp 请求方法
     * 并且使用 trait HttpRequest request 方法
     */
    use HttpRequest {
        request as TencentRequest;
    }

    /**
     * @var \Ow\MarketingApi\Tencent\Kernel\ServiceContainer
     */
    protected $app;

    /**
     * 账号access_token account_id
     * @var array
     */
    protected $owner = [];

    /**
     * 默认全局配置
     * @var array
     */
    protected $defaults = [
        'headers' => [
            'Content-Type' => 'application/json',
        ],
        'http' => [
            'timeout' => 10,
            'base_uri' => 'https://api.e.qq.com/v1.3/'
        ]
    ];

    /**
     * 基础参数
     * @var array
     */
    protected $commonParameters = [];

    /**
     * BaseHttpClient constructor.
     * @param $app
     */
    public function __construct($app)
    {
        $this->app = $app;

        $this->owner = [
            "account_id" => $app->defaultConfig["owner_id"],
            "access_token" => $app->defaultConfig["access_token"],
        ];

        $this->commonParameters($this->owner["access_token"]);

    }

    /**
     * @throws TencentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param array $parameters
     * @param string $method
     * @param string $url
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function request(string $method, string $url, array $parameters = [])
    {
        //实例化的时候传入了广告主id, 赋值到请求的参数里
        $parameters["account_id"] = $this->owner["account_id"];

        //获取请求路由附加参数,腾讯验证在路由里
        $url = $this->verificationRequestUri($method, $parameters, $url);

        //遍历参数，数组需要转为字符串
        $parameters = $this->verificationRequestParameters($method, $parameters);

        return $this->TencentRequest($method, $url, $parameters);
    }

    /**
     * 验证请求类型 获取请求路由参数
     * get  基础参数需要与请求参数合并
     * post 只需要获取基础参数
     * @param $method
     * @param $parameters
     * @param $url
     * @return string
     */
    public function verificationRequestUri($method, $parameters, $url)
    {
        $httpBasicInfo = $this->defaults["http"];

        $url = $httpBasicInfo["base_uri"] . "/" . $url;

        if (strtoupper($method) === "GET") {
            $url .= "?" . http_build_query(array_merge($this->commonParameters, $parameters));
        } else {
            $url .= "?" . http_build_query($this->commonParameters);
        }

        return $url;
    }

    /**
     * 素材上传
     * @throws TencentException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @param $url
     * @param $parameters
     * @return mixed
     */
    public function multipartUpload($url, $parameters)
    {
        $parameters["account_id"] = $this->owner["account_id"];

        $multipart = [];

        foreach ($parameters as $key => $value) {
            $multipart[] = ["name" => $key, "contents" => $value];
        }

        return $this->TencentRequest("POST", $url, ['multipart' => $multipart]);
    }

    /**
     * 验证请求参数，数组需要转为字符串
     * @param $method
     * @param $parameters
     * @return array
     */
    public function verificationRequestParameters($method, $parameters)
    {
        //腾讯请求参数需要转字符串
        if ($parameters) {
            foreach ($parameters as $key => $value) {
                if (!is_string($value)) {
                    $parameters[$key] = json_encode($value);
                }
            }
        }

        //如果是post, 确保 GuzzleHttp 请求参数
        if ($method === "POST") {
            $parameters = ["json" => $parameters];
        }

        return $parameters;
    }

    /**
     * 组合请求路由的基础参数
     * @param $access_token
     */
    public function commonParameters($access_token)
    {
        $this->commonParameters = [
            "nonce" => md5(uniqid("zs_lee_gdt", true)),
            "timestamp" => time(),
            "access_token" => $access_token,
        ];
    }
}