<?php

namespace Ow\MarketingApi\Tencent\Ad;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class AdServiceProvider implements ServiceProviderInterface
{

    /**
     * @inheritDoc
     */
    public function register(Container $pimple)
    {
        $pimple['ad'] = function ($app) {
            return new Ad($app);
        };
    }
}