<?php

namespace Ow\MarketingApi\Tencent;

use Ow\MarketingApi\Tencent\Ad\AdServiceProvider;
use Ow\MarketingApi\Tencent\Campaign\CampaignServiceProvider;
use Ow\MarketingApi\Tencent\Creative\CreativeServiceProvider;
use Ow\MarketingApi\Tencent\Kernel\Cache\CacheServiceProvider;
use Ow\MarketingApi\Tencent\Kernel\ServiceContainer;
use Ow\MarketingApi\Tencent\Material\MaterialServiceProvider;
use Ow\MarketingApi\Tencent\Oauth\OauthServiceProvider;
use Ow\MarketingApi\Tencent\Pages\PagesServiceProvider;
use Ow\MarketingApi\Tencent\Plan\PlanServiceProvider;
use Ow\MarketingApi\Tencent\ProductCatalogs\ProductCatalogsProvider;
use Ow\MarketingApi\Tencent\Report\ReportServiceProvider;

/**
 * Class Application
 * @property \Ow\MarketingApi\Tencent\Oauth\Oauth $oauth
 * @property \Ow\MarketingApi\Tencent\Campaign\Campaign $campaign
 * @property \Ow\MarketingApi\Tencent\Plan\Plan $pan
 * @property \Ow\MarketingApi\Tencent\Creative\Creative $creative
 * @property \Ow\MarketingApi\Tencent\Ad\Ad $ad
 * @property \Ow\MarketingApi\Tencent\Report\Report $report
 * @property \Ow\MarketingApi\Tencent\Material\Brand $brand
 * @property \Ow\MarketingApi\Tencent\Material\Image $image
 * @property \Ow\MarketingApi\Tencent\Material\Video $video
 * @property \Ow\MarketingApi\Tencent\ProductCatalogs\ProductCatalogs $product_catalogs
 * @property \Ow\MarketingApi\Tencent\Pages\Pages $page
 * @property \Ow\MarketingApi\Tencent\Kernel\Cache\Cache $cache
 * @package Ow\MarketingApi\Tencent
 */

class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        OauthServiceProvider::class,
        CampaignServiceProvider::class,
        PlanServiceProvider::class,
        CreativeServiceProvider::class,
        AdServiceProvider::class,
        ReportServiceProvider::class,
        MaterialServiceProvider::class,
        CacheServiceProvider::class,
        PagesServiceProvider::class,
        ProductCatalogsProvider::class
    ];
}
